/** 
 * Copyright 2004-2010 DTRules.com, Inc.
 *   
 * Licensed under the Apache License, Version 2.0 (the "License");  
 * you may not use this file except in compliance with the License.  
 * You may obtain a copy of the License at  
 *   
 *      http://www.apache.org/licenses/LICENSE-2.0  
 *   
 * Unless required by applicable law or agreed to in writing, software  
 * distributed under the License is distributed on an "AS IS" BASIS,  
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
 * See the License for the specific language governing permissions and  
 * limitations under the License.  
 **/ 

package pl.me.dtrules;

import com.dtrules.testsupport.ITestHarness;
import excel.util.Excel2XML;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import static pl.me.dtrules.Measurer.cpu;
import static pl.me.dtrules.Measurer.memoryUsage;
import static pl.me.dtrules.Measurer.processorUsage;


/**
 * @author Paul Snow
 *
 */
public class CompileAndRun {

    /**
     * In Eclipse, System.getProperty("user.dir") returns the project
     * directory.  We add a slash to insure the path ends with a slash.
     */
    public static String path    = System.getProperty("user.dir")+"\\";
    
    /**
     * Routine to compile decision tables.
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception { 
        try {
            //compiling and testing
            new Thread(new Measurer()).start();
            while (cpu == -1) {
                System.out.println("Waiting for measuring thread...");
                Thread.sleep(3000);
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt",true));

            System.out.println(path);
            Date start = new Date();
            Excel2XML.compile(path,"DTRules.xml","DefaultRuleSet","repository");
            ITestHarness t = new EngineTest();
            t.runTests();
            Date end = new Date();
            Measurer.ifContinue = false;

            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Double [] proc = processorUsage.toArray(new Double[processorUsage.size()]);
            double minProc = proc[0];
            double maxProc = proc[0];
            for (double p : proc) {
                if (p < minProc && p != Double.NaN) minProc = p;
                if (p > maxProc && p != Double.NaN) maxProc = p;
            }
            try {
                bw.write(printDateTime());
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf(maxProc-minProc));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
                       
        } catch ( Exception ex ) {
            System.out.println("Failed to convert the Excel files");
            ex.printStackTrace();
            throw ex;
        }
     }

    private static String printDateTime() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }
}
